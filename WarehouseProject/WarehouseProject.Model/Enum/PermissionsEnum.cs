﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProject.Model.Enum
{
    public enum PermissionsEnum
    {
        ControlUsers = 1 << 0,
        ControlItems = 1 << 1,
        ControlAreasLines = 1 << 2,
        ControlVoucher = 1 << 3,

        All = ControlUsers | ControlAreasLines | ControlItems | ControlVoucher
    }
}
