﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProject.Model.Enum
{
    public enum StatusEnum
    {
        Pending =1<<0,
        Delivered = 1<<1,
        Canceled = 1<<2,
    }
}
