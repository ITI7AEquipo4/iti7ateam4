﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseProject.Model.Model
{
    public class Item : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A name has to be set.", ErrorMessageResourceName = "Name")]
        string Name { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public int MinStock { get; set; }
        public int MaxStock { get; set; }
        public int MarginDays { get; set; }
        public decimal Price { get; set; }
        public Provider ItemProvider { get; set; }
    }
}
