﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseProject.Model.Model
{
    public class ItemVoucher : Item
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe existir un ID de voucher", ErrorMessageResourceName = "VoucherIDRequired")]
        [Min(0,ErrorMessage ="IDs por debajo de 0 no son validos",ErrorMessageResourceName ="VoucherIDMin")]
        public int VoucherID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe existir un ID de consumible", ErrorMessageResourceName = "ItemIDRequired")]
        [Min(0, ErrorMessage = "IDs por debajo de 0 no son validos", ErrorMessageResourceName = "ItemIDMin")]
        public int ItemID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar una cantidad para cada consumible", ErrorMessageResourceName = "QtyRequired")]
        [Min(1, ErrorMessage = "La cantidad de item no puede ser menor a 1", ErrorMessageResourceName = "QtyMIN")]
        public int Qty { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe existir un subtotal", ErrorMessageResourceName = "SubTotalRequired")]
        [Min(1, ErrorMessage = "El subtotal no puede ser menor a 1", ErrorMessageResourceName = "SubTotalMin")]
        public decimal SubTotal { get; set; }
    }
}
