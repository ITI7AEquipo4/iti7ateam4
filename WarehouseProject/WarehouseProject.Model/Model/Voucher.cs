﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WarehouseProject.Model.Enum;

namespace WarehouseProject.Model.Model
{
    public class Voucher : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Voucher necesita un folio", ErrorMessageResourceName = "InvoiceNumber")]
        [MinLength(15, ErrorMessage = "El folio no debe contener 15 caracteres", ErrorMessageResourceName = "InvoiceNumber")]
        public int InvoiceNumber { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de creacion del voucher no puede estar vacia.")]
        public DateTime? OrderDate { get; set; }
        public DateTime? DeliverDate { get; set; }
        [MaxLength(100, ErrorMessage = "El mensaje no puede contener mas de 100 caracteres", ErrorMessageResourceName = "Message")]
        public string Comment { get; set; }
        [Required(ErrorMessage = "El voucher debe contener la informacion del requisitor", ErrorMessageResourceName = "OrderUser")]
        public int OrderUser { get; set; }
        [Min(0)]
        public int? DeliverUser { get; set; }
        [Required(ErrorMessage = "El subtotal no puede estar vacio", ErrorMessageResourceName = "SubTotal")]
        public decimal Total { get; set; }
        [Required(ErrorMessage = "El voucher debe tener un estado establecido", ErrorMessageResourceName = "Status")]
        public StatusEnum Status { get; set; }

    }
}
