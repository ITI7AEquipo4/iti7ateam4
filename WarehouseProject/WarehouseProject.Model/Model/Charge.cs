﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WarehouseProject.Model.Enum;

namespace WarehouseProject.Model.Model
{
    public class Charge : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A name has to be set.", ErrorMessageResourceName = "Name")]
        string Name { get; set; }
        public PermissionsEnum Permissions { get; set; }
        public string Status { get; set; }

    }
}
