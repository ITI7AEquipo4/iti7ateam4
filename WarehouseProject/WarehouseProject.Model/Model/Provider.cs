﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseProject.Model.Model
{
    public class Provider
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A name has to be set.", ErrorMessageResourceName = "Name")]
        [StringLength(30, MinimumLength = 2)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Ingresa un correo electronico", ErrorMessageResourceName = "Correo")]
        [StringLength(30, MinimumLength = 10, ErrorMessage = "El correo debe tener almenos 12 caracteres y no mas de 30")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "El formato del correo electronico no es el adecuado")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Ingresa un Numero telefonico", ErrorMessageResourceName = "Telefono")]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "El telefono debe tener almenos 8 caracteres y no mas de 16")]
        [RegularExpression(@"([(+]*[0-9]*[() [0-9-.]*[ 0-9 ])", ErrorMessage = "El formato del telefono no es el adecuado.")]
        public string PhoneNumber { get; set; }
    }
} 

