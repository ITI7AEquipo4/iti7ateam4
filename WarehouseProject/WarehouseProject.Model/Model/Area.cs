﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseProject.Model.Model
{
    public class Area : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A name has to be set.", ErrorMessageResourceName = "Name")]
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
