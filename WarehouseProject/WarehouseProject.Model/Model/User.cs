﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseProject.Model.Model
{
    public class User : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A name has to be set.", ErrorMessageResourceName = "Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "A Charge has to be set.")]
        public Charge ChargeId { get; set; }


        [Required(ErrorMessage = "A Status has to be set.")]
        public bool Status { get; set; }
    }
}
