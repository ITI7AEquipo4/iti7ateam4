﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseProject.Model.Model;

namespace WarehouseProject.Repository.Repository
{

    public interface IProviderRepository
    {
        IEnumerable<Provider> GetAllProviders();
        Provider GetProviderById(int id);
        int InsertProvider(Provider provider);
        int UpdateProvider(Provider provider);


    }
    class ProviderRepository : IProviderRepository
    {
        public IEnumerable<Provider> GetAllProviders()
        {
            throw new NotImplementedException();
        }

        public Provider GetProviderById(int id)
        {
            throw new NotImplementedException();
        }

        public int InsertProvider(Provider provider)
        {
            throw new NotImplementedException();
        }

        public int UpdateProvider(Provider provider)
        {
            throw new NotImplementedException();
        }
    }
}
