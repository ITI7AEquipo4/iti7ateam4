﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseProject.Model.Model;

namespace WarehouseProject.Repository.Data
{
    class DataDbContext : DbContext
    {
        public DataDbContext()
        {

        }
        public DataDbContext(DbContextOptions<DataDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //En caso de que el contexto no este configurado, lo configuramos mediante la cadena de conexion
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=localhost\\SQLEXPRESS;Initial Catalog=TestDB;User ID=sa;Password=sa");
            }
        }

        //Tablas de datos
        public virtual DbSet<Voucher> Vouchers { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }

    }
}
