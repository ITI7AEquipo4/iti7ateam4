﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WarehouseProject.Model.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WarehouseProject.Controllers
{
    [Route("api/[controller]")]
    public class VoucherController : Controller
    {
        // GET: api/<controller>
        [HttpGet, ActionName("GetAllVouchers")]
        public ActionResult<Voucher> Get()
        {
            throw new NotImplementedException();
        }

        // GET api/<controller>/5
        [HttpGet("{id}"), ActionName("GetVoucherByID")]
        public ActionResult Get(int id)
        {
            throw new NotImplementedException();

        }

        // POST api/<controller>
        [HttpPost, ActionName("PostVoucher")]
        public void Post([FromBody]Voucher voucher)
        {
            throw new NotImplementedException();

        }

        // PUT api/<controller>/5
        [HttpPut("{id}"), ActionName("PutVoucher")]
        public ActionResult Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}", Name = "LogicalDelete")]
        public ActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
