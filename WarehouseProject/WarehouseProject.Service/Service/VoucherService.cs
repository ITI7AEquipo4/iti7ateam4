﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseProject.Model.Model;

namespace WarehouseProject.Service.Service
{
    public interface IVoucherService
    {
        List<Voucher> GetAllVouchers();
        List<Voucher> GetPendingVouchers();
        List<Voucher> GetCanceledVouchers();
        List<Voucher> GetDeliveredVouchers();
    }
    class VoucherService : IVoucherService
    {
        public List<Voucher> GetAllVouchers()
        {

            throw new NotImplementedException();
        }

        public List<Voucher> GetPendingVouchers()
        {
            throw new NotImplementedException();
        }

        public List<Voucher> GetCanceledVouchers()
        {
            throw new NotImplementedException();
        }

        public List<Voucher> GetDeliveredVouchers()
        {
            throw new NotImplementedException();
        }
    }
}
