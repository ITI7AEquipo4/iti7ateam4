﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseProject.Model.Model;

namespace WarehouseProject.Service.Service
{

    public interface IProviderService
    {
        List<Provider> GetAllProviders();
        Provider GetProviderById(int id);
        string InsertProvider(Provider provider);
        string UpdateProvider(Provider provider);
       
    }
    class ProviderService : IProviderService
    {
        public List<Provider> GetAllProviders()
        {
            throw new NotImplementedException();
        }

        public Provider GetProviderById(int id)
        {
            throw new NotImplementedException();
        }

        public string InsertProvider(Provider provider)
        {
            throw new NotImplementedException();
        }

        public string UpdateProvider(Provider provider)
        {
            throw new NotImplementedException();
        }
    }
}
